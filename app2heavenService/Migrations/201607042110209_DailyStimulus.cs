namespace app2heavenService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DailyStimulus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DailyStimulus", "Language_Code", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DailyStimulus", "Language_Code");
        }
    }
}
