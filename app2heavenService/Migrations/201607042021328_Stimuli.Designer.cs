// <auto-generated />
namespace app2heavenService.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Stimuli : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Stimuli));
        
        string IMigrationMetadata.Id
        {
            get { return "201607042021328_Stimuli"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
