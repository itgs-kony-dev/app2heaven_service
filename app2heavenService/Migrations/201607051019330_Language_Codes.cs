namespace app2heavenService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Language_Codes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CommunityExperiences", "Language_Code", c => c.String());
            AddColumn("dbo.Examinations", "Language_Code", c => c.String());
            AddColumn("dbo.MonthlyPrayerRequests", "Language_Code", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MonthlyPrayerRequests", "Language_Code");
            DropColumn("dbo.Examinations", "Language_Code");
            DropColumn("dbo.CommunityExperiences", "Language_Code");
        }
    }
}
