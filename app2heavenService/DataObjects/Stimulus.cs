﻿using System;
using System.Data.Entity;

using Microsoft.Azure.Mobile.Server;

namespace app2heavenService.DataObjects
{
    public class Stimulus : EntityData
    {
        public string Title { get; set; }

        public string Text { get; set; }

        public DateTimeOffset ValidFrom { get; set; }

        public DateTimeOffset ValidUntil { get; set; }

        public string User_Id { get; set; }

        public string Community_Id { get; set; }

        public string Category { get; set; }

        public int Cms_Id { get; set; }

        public string Author { get; set; }
                
        public string Language_Code { get; set; }
    }

    public class Context2 : DbContext
    {

    }
}