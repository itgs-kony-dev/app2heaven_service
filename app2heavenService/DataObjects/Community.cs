﻿using Microsoft.Azure.Mobile.Server;

namespace app2heavenService.DataObjects
{
    public class Community : EntityData
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string CommunityType_Id { get; set; }

        public int Cms_Id { get; set; }

        public string Url { get; set; }        
    }
}