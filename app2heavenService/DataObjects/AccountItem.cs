﻿using Microsoft.Azure.Mobile.Server;

namespace app2heavenService.DataObjects
{
    public class AccountItem : EntityData
    {
        public string Identification { get; set; }

        public string GSM { get; set; }

        public int Code { get; set; }

        public string Key { get; set; }
    }
}