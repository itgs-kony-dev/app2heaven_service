﻿using Microsoft.Azure.Mobile.Server;

namespace app2heavenService.DataObjects
{
    public class Language : EntityData
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public bool Published { get; set; }
    }
}