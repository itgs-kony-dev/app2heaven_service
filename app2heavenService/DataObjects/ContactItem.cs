﻿using Microsoft.Azure.Mobile.Server;

namespace app2heavenService.DataObjects
{
    public class ContactItem : EntityData
    {
        public string GSM { get; set; }

        public string Json { get; set; }
    }
}