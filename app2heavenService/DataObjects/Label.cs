﻿using Microsoft.Azure.Mobile.Server;

namespace app2heavenService.DataObjects
{
    public class Label : EntityData
    {
        public string Label_Id { get; set; }

        public string Language_Code { get; set; }

        public string Value { get; set; }
    }
}