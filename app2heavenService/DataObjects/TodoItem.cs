﻿using Microsoft.Azure.Mobile.Server;

namespace app2heavenService.DataObjects
{
    public class TodoItem : EntityData
    {
        public string Text { get; set; }

        public bool Complete { get; set; }
    }
}