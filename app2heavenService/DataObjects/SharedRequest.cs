﻿using Microsoft.Azure.Mobile.Server;

namespace app2heavenService.DataObjects
{
    public class SharedRequest: EntityData
    {
        public string From { get; set; }

        public string To { get; set; }

        public string Text { get; set; }

        public string Title { get; set; }

        public bool Read { get; set; }
        
        public string Parent { get; set; }

        public bool Done { get; set; }
    }
}