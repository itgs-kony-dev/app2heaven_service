﻿using Microsoft.Azure.Mobile.Server;

namespace app2heavenService.DataObjects
{
    public class MessageItem : EntityData
    {
        public string From { get; set; }

        public string To { get; set; }

        public string Text { get; set; }
    }
}