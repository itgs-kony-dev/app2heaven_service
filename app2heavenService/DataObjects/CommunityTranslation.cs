﻿using System;
using System.Data.Entity;

using Microsoft.Azure.Mobile.Server;

namespace app2heavenService.DataObjects
{
    public class CommunityTranslation : EntityData
    {
        public string Language_Code { get; set; }

        public string Community_Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Url { get; set; }
    }
}