﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.Mobile.Server.Tables;
using app2heavenService.DataObjects;

namespace app2heavenService.Models
{
    public class app2heavenContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to alter your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        private const string connectionStringName = "Name=MS_TableConnectionString";

        public app2heavenContext() : base(connectionStringName)
        {
        } 

        public DbSet<TodoItem> TodoItems { get; set; }
        public DbSet<AccountItem> AccountItems { get; set; }
        public DbSet<ContactItem> ContactItems { get; set; }
        public DbSet<MessageItem> MessageItems { get; set; }
        public DbSet<Label> Labels { get; set; }
        public DbSet<Stimulus> Stimuli { get; set; }
        public DbSet<Community> Communities { get; set; }
        public DbSet<DailyStimulus> DailyStimuli { get; set; }
        public DbSet<MonthlyPrayerRequest> MonthlyPrayerRequests { get; set; }
        public DbSet<CommunityExperience> CommunityExperiences { get; set; }
        public DbSet<Examination> Examinations { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<SharedExperience> SharedExperiences { get; set; }
        public DbSet<SharedRequest> SharedRequests { get; set; }
        public DbSet<SharedWord> SharedWord { get; set; }
        public DbSet<CommunityTranslation> CommunityTranslations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add(
                new AttributeToColumnAnnotationConvention<TableColumnAttribute, string>(
                    "ServiceTableColumn", (property, attributes) => attributes.Single().ColumnType.ToString()));
        }        
    }
}
