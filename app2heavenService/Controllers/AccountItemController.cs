﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using System.Collections.Generic;
using System.Diagnostics;

using AutoMapper;
using AutoMapper.QueryableExtensions;

using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.Mobile.Server.Config;

using Twilio;

using app2heavenService.DataObjects;
using app2heavenService.Models;


namespace app2heavenService.Controllers
{
    public class AccountItemController : TableController<AccountItem>
    {
        private app2heavenContext context;

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            this.context = new app2heavenContext();
            //context.AccountItems.()
            DomainManager = new EntityDomainManager<AccountItem>(context, Request);
        }

        // GET tables/AccountItem
        public IQueryable<AccountItem> GetAllAccountItems()
        {
            /*var deleted = Query().Where(x => x.Text != "John");

            foreach (AccountItem item in deleted)
            {
                item.Deleted = true;
                item.UpdatedAt = DateTimeOffset.Now;
            }*/

            return Query();//.Where(x => x.Text == "John");
            
               // .Union(deleted)
               // .Union(new List<AccountItem> { new AccountItem { Text = "From Service", CreatedAt = DateTimeOffset.Now, Id = Guid.NewGuid().ToString(), UpdatedAt = DateTimeOffset.Now, Version = new byte[] { 8, 239 } } });                
        }

        // GET tables/AccountItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<AccountItem> GetAccountItem(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/AccountItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<AccountItem> PatchAccountItem(string id, Delta<AccountItem> patch)
        {
            AccountItem item = patch.GetEntity();
            PushNotifier pn = new PushNotifier(this.Configuration);

            var oldItems = this.context.AccountItems;

            AccountItem oldItem = oldItems
                .Where(x => x.Id == id)
                .FirstOrDefault();

            /*if (oldItem != null && oldItem.Code == null)
            {
                Task.Run(async () => {
                    var notif = "{ \"data\" : {\"message\":\"" + "Fehler, Bitte neuen Code anfordern" + "\"}}";
                    await pn.SendPushNotification(notif, new string[] { id });
                });

                //return new Task<AccountItem>(;
            }*/

            if (item.Code == oldItem.Code && item.Code != 0 && string.IsNullOrEmpty(item.Key))
            {
                Task.Run(async () => {
                    var notif = "{ \"data\" : {\"message\":\"" + "Sie sind angemeldet" + "\", "
                        + "\"title\":\"" + "Herzlich Willkommen" + "\"}}";
                    await pn.SendPushNotification(notif, new string[] { id });
                }).Wait();

               // Task.Run(async () => {
                    try
                    {
                        foreach(AccountItem deleteItem in oldItems.Where(x => x.GSM == item.GSM))
                        {
                            if (deleteItem.Id != item.Id)
                                //this.context.AccountItems.Remove(deleteItem);
                                deleteItem.Deleted = true;
                                //await DeleteAsync(deleteItem.Id);
                        }
                    }
                    catch (Exception exception)
                    {
                        Trace.TraceInformation("Exception: " + exception);
                    }
                //}).Wait();

                item.Key = "558";
            }
            else if (item.Code != oldItem.Code && item.Code != 0 && string.IsNullOrEmpty(item.Key))
            {
                Task.Run(async () => {
                    var notif = "{ \"data\" : {\"message\":\"" + "Falsch, bitte nochmal probieren" + "\"}}";
                    await pn.SendPushNotification(notif, new string[] { id });
                });

                item.Identification = oldItem.Identification;
                item.GSM = oldItem.GSM;
                item.Key = "";
                item.Code = oldItem.Code;             
            }
            else
            {               
                item.Code = new Random().Next(100000, 999999);
                Trace.TraceInformation("New Code: " + item.Code.ToString());

                string AccountSid = "AC2686b286843eaafde1f4f98308339303";
                string AuthToken = "1b10f2e9d0fba36439de20d16b104dd7";
                var twilio = new TwilioRestClient(AccountSid, AuthToken);

                //Message twilioResult = twilio.SendMessage("+4915735982184", item.GSM, "New Code: " + item.Code.ToString());
                Trace.TraceInformation("SMS: " + "+4915735982184 " + item.GSM + " - " + "New Code: " + item.Code.ToString());
            }

            return UpdateAsync(id, patch);
        }

        // POST tables/AccountItem
        public async Task<IHttpActionResult> PostAccountItem(AccountItem item)
        {
            item.Code = new Random().Next(100000, 999999);
            item.Key = null;

            //string AccountSid = "AC2686b286843eaafde1f4f98308339303";
            //string AuthToken = "1b10f2e9d0fba36439de20d16b104dd7";
            string AccountSid = "AC662bb0eca00b67b97e28af59ab838315";
            string AuthToken = "a25191c53742f641a87d51b3827eb35c"; 
            var twilio = new TwilioRestClient(AccountSid, AuthToken);

            Message twilioResult = twilio.SendMessage("+4915735986125", item.GSM, "Code: " + item.Code.ToString());
            Trace.TraceInformation("SMS: " + "+4915735986125 " + item.GSM + " - " + "Code: " + item.Code.ToString());

            AccountItem current = await InsertAsync(item);
             
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/AccountItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteAccountItem(string id)
        {
            return DeleteAsync(id);
        }        
    }
}