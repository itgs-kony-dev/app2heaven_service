﻿using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using System.Diagnostics;

using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.Mobile.Server.Config;

using app2heavenService.DataObjects;
using app2heavenService.Models;

namespace app2heaven_dev2Service.Controllers
{
    public class ExaminationController : TableController<Examination>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            app2heavenContext context = new app2heavenContext();

            DomainManager = new EntityDomainManager<Examination>(context, Request);
        }

        // GET tables/MonthlyPrayerRequest
        public IQueryable<Examination> GetAllExaminations()
        {
            return Query();
        }

        // GET tables/MonthlyPrayerRequest/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Examination> GeExamination(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/MonthlyPrayerRequest/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Examination> PatchMonthlyPrayerRequest(string id, Delta<Examination> patch)
        {
            return UpdateAsync(id, patch);
        }

        /*
        // POST tables/MonthlyPrayerRequest
        public async Task<IHttpActionResult> PostMonthlyPrayerRequest(Examination item)
        {
            MonthlyPrayerRequest current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/MonthlyPrayerRequest/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteMonthlyPrayerRequest(string id)
        {
            return DeleteAsync(id);
        }*/
    }
}