﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using System.Diagnostics;
using Microsoft.Azure.Mobile.Server;
using app2heavenService.DataObjects;
using app2heavenService.Models;

using System.Collections.Generic;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.Mobile.Server.Config;

using System;
using System.Collections.ObjectModel;
using Twilio;

namespace app2heavenService.Controllers
{
    public class ContactItemController : TableController<ContactItem>
    {
        List<ContactItem> newItems;
        app2heavenContext context;

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            this.context = new app2heavenContext();
            //context.ContactItems.()
            DomainManager = new EntityDomainManager<ContactItem>(context, Request);
        }

        // GET tables/ContactItem
        public IQueryable<ContactItem> GetAllContactItems()
        {
            /*var deleted = Query().Where(x => x.Text != "John");

            foreach (ContactItem item in deleted)
            {
                item.Deleted = true;
                item.UpdatedAt = DateTimeOffset.Now;
            }*/
            //return new app2heavenContext().ContactItems.Take(50);
            if (newItems == null)
            {
                Trace.TraceInformation("newItems == null");

                return null;
            }
            else
            {
                Trace.TraceInformation(newItems.Count.ToString());

                return newItems.AsQueryable<ContactItem>();
            }
            //return Query();
            //return Query().Where(x => string.IsNullOrEmpty(x.Json));//.Where(x => x.Text == "John");
            
               // .Union(deleted)
               // .Union(new List<ContactItem> { new ContactItem { Text = "From Service", CreatedAt = DateTimeOffset.Now, Id = Guid.NewGuid().ToString(), UpdatedAt = DateTimeOffset.Now, Version = new byte[] { 8, 239 } } });                
        }

        // GET tables/ContactItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ContactItem> GetContactItem(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ContactItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ContactItem> PatchContactItem(string id, Delta<ContactItem> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/ContactItem
        public async Task<IHttpActionResult> PostContactItem(ContactItem item)
        {
            ContactItem[] contactItems = Newtonsoft.Json.JsonConvert.DeserializeObject<ContactItem[]>(item.Json);
            newItems = new List<ContactItem>();

            //Dictionary<string, object> dict = new Dictionary<string, object>();

            foreach (ContactItem contact in contactItems)
            {               
                //contact.Id = new Guid().ToString()
                //dict.Add(contact.Id, new { Id = contact.Id });
                newItems.Add(await InsertAsync(contact));
                //contact.Id = new Guid().ToString();
                //newItems.Add(contact);
            }

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(newItems.ToArray());
            ContactItem current = new ContactItem { Id = new Guid().ToString(), Json = json };

            //return CreatedAtRoute("Tables", dict, current);
            //return CreatedAtRoute("Tables", new { id = current.Id }, current);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
            //return Ok(newItems);
            //new TaskCompletionSource<IHttpActionResult>().SetResult(IHttpActionResult)
        }

        // DELETE tables/ContactItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteContactItem(string id)
        {
            return DeleteAsync(id);
        }
    }
}