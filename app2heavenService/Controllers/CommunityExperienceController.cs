﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using System.Diagnostics;

using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.Mobile.Server.Config;

using app2heavenService.DataObjects;
using app2heavenService.Models;

namespace app2heaven_dev2Service.Controllers
{
    public class CommunityExperienceController : TableController<CommunityExperience>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            app2heavenContext context = new app2heavenContext();
            DomainManager = new EntityDomainManager<CommunityExperience>(context, Request);
        }

        // GET tables/TodoItem
        public IQueryable<CommunityExperience> GetAllSharedExperienceItems()
        {
            return Query();
        }

        // GET tables/TodoItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<CommunityExperience> GetSharedExperienceItem(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/TodoItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<CommunityExperience> PatchSharedExperienceItem(string id, Delta<CommunityExperience> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/TodoItem
        /*public async Task<IHttpActionResult> PostSharedExperienceItem(CommunityExperience item)
        {
            CommunityExperience current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/TodoItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteSharedExperienceItem(string id)
        {
            return DeleteAsync(id);
        }*/
    }
}