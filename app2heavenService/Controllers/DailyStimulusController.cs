﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using System.Diagnostics;

using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.Mobile.Server.Config;

using app2heavenService.DataObjects;
using app2heavenService.Models;

namespace app2heavenService.Controllers
{
    public class DailyStimulusController : TableController<DailyStimulus>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            app2heavenContext context = new app2heavenContext();

            DomainManager = new EntityDomainManager<DailyStimulus>(context, Request);
        }

        // GET tables/Stimulus
        public IQueryable<DailyStimulus> GetAllDayliStimuli()
        {
            return Query();
        }

        // GET tables/Stimulus/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<DailyStimulus> GetDayliStimulus(string id)
        {
            return Lookup(id);
        }

        
        // PATCH tables/Stimulus/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<DailyStimulus> PatchStimulus(string id, Delta<DailyStimulus> patch)
        {
            return UpdateAsync(id, patch);
        }
         
    }
}