﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using app2heavenService.DataObjects;
using app2heavenService.Models;

namespace app2heavenService.Controllers
{
    public class TodoItem2Controller : ApiController
    {
        private app2heavenContext db = new app2heavenContext();

        // GET api/TodoItem2
        public IQueryable<TodoItem> GetTodoItems()
        {
            return db.TodoItems;
        }

        // GET api/TodoItem2/5
        [ResponseType(typeof(TodoItem))]
        public IHttpActionResult GetTodoItem(string id)
        {
            TodoItem todoitem = db.TodoItems.Find(id);
            if (todoitem == null)
            {
                return NotFound();
            }

            return Ok(todoitem);
        }

        // PUT api/TodoItem2/5
        public IHttpActionResult PutTodoItem(string id, TodoItem todoitem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != todoitem.Id)
            {
                return BadRequest();
            }

            db.Entry(todoitem).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                if (!TodoItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/TodoItem2
        [ResponseType(typeof(TodoItem))]
        public IHttpActionResult PostTodoItem(TodoItem todoitem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TodoItems.Add(todoitem);

            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {
                if (TodoItemExists(todoitem.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = todoitem.Id }, todoitem);
        }

        // DELETE api/TodoItem2/5
        [ResponseType(typeof(TodoItem))]
        public IHttpActionResult DeleteTodoItem(string id)
        {
            TodoItem todoitem = db.TodoItems.Find(id);
            if (todoitem == null)
            {
                return NotFound();
            }

            db.TodoItems.Remove(todoitem);
            db.SaveChanges();

            return Ok(todoitem);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TodoItemExists(string id)
        {
            return db.TodoItems.Count(e => e.Id == id) > 0;
        }
    }
}