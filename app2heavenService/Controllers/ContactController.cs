﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;

using app2heavenService.DataObjects;
using app2heavenService.Models;

namespace app2heavenService.Controllers
{
    public class ContactController : ApiController
    {
        private app2heavenContext context = new app2heavenContext();

        [HttpPost]
        public async Task<string[]> Post([FromBody]string[] myHandle)
        {
            //return "Hallo Welt " + handle[0].ToString() + " from: " + handle[1].ToString();

            //List<string> contacts = new List<string>();

            List<string> intersections = this.context.AccountItems
                .Select(x => x.GSM)
                .Intersect(myHandle.ToList())
                .ToList();

            //foreach (ContactItem item in this.context.ContactItems)
            //    contacts.Add(item.GSM);

            return intersections.ToArray();
        }
    }
}
