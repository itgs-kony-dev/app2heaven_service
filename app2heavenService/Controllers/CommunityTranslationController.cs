﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using System.Diagnostics;

using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.Mobile.Server.Config;

using app2heavenService.DataObjects;
using app2heavenService.Models;

namespace app2heaven_dev2Service.Controllers
{
    public class CommunityTranslationController : TableController<CommunityTranslation>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            app2heavenContext context = new app2heavenContext();

            DomainManager = new EntityDomainManager<CommunityTranslation>(context, Request);
        }

        public IQueryable<CommunityTranslation> GetAllCommunityTranslations()
        {
            return Query();
        }

        public SingleResult<CommunityTranslation> GetCommunityTranslation(string id)
        {
            return Lookup(id);
        }

        public Task<CommunityTranslation> PatchCommunityTranslation(string id, Delta<CommunityTranslation> patch)
        {
            return UpdateAsync(id, patch);
        }
    }
}