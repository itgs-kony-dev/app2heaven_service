﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using app2heavenService.DataObjects;
using app2heavenService.Models;

using System.Collections.Generic;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.Mobile.Server.Config;

using System;
using Twilio;

namespace app2heavenService.Controllers
{
    public class TodoItemController : TableController<TodoItem>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            app2heavenContext context = new app2heavenContext();
            //context.TodoItems.()
            DomainManager = new EntityDomainManager<TodoItem>(context, Request);
        }

        // GET tables/TodoItem
        public IQueryable<TodoItem> GetAllTodoItems()
        {
            /*var deleted = Query().Where(x => x.Text != "John");

            foreach (TodoItem item in deleted)
            {
                item.Deleted = true;
                item.UpdatedAt = DateTimeOffset.Now;
            }*/

            return Query();//.Where(x => x.Text == "John");
            
               // .Union(deleted)
               // .Union(new List<TodoItem> { new TodoItem { Text = "From Service", CreatedAt = DateTimeOffset.Now, Id = Guid.NewGuid().ToString(), UpdatedAt = DateTimeOffset.Now, Version = new byte[] { 8, 239 } } });                
        }

        // GET tables/TodoItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<TodoItem> GetTodoItem(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/TodoItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<TodoItem> PatchTodoItem(string id, Delta<TodoItem> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/TodoItem
        public async Task<IHttpActionResult> PostTodoItem(TodoItem item)
        {
            TodoItem current = await InsertAsync(item);

            // Get the settings for the server project.
            HttpConfiguration config = this.Configuration;
            MobileAppSettingsDictionary settings =
                this.Configuration.GetMobileAppSettingsProvider().GetMobileAppSettings();

            // Get the Notification Hubs credentials for the Mobile App.
            string notificationHubName = settings.NotificationHubName;
            string notificationHubConnection = settings
                .Connections[MobileAppSettingsKeys.NotificationHubConnectionString].ConnectionString;

            // Create a new Notification Hub client.
            NotificationHubClient hub = NotificationHubClient
                .CreateClientFromConnectionString(notificationHubConnection, notificationHubName);

            // Sending the message so that all template registrations that contain "messageParam"
            // will receive the notifications. This includes APNS, GCM, WNS, and MPNS template registrations.
            Dictionary<string, string> templateParams = new Dictionary<string, string>();
            templateParams["messageParam"] = item.Text + " was added to the list.";

            try
            {
                // Send the push notification and log the results.
                //Microsoft.WindowsAzure.MobileServices.Push
                //var result = await hub.SendTemplateNotificationAsync(templateParams, new string [] { "John" });
                var notif = "{ \"data\" : {\"message\":\"" + "From " + "user" + ": " + "message" + "\"}}";
                var result = await hub.SendGcmNativeNotificationAsync(notif, new string[] { item.Text });

                /*string AccountSid = "AC2686b286843eaafde1f4f98308339303";
                string AuthToken = "1b10f2e9d0fba36439de20d16b104dd7";
                var twilio = new TwilioRestClient(AccountSid, AuthToken);

                var message = twilio.SendMessage("+4915735982184", "+491795876967", item.Text);*/

                // Write the success result to the logs.
                config.Services.GetTraceWriter().Info(result.State.ToString());
            }
            catch (System.Exception ex)
            {
                // Write the failure result to the logs.
                config.Services.GetTraceWriter()
                    .Error(ex.Message, null, "Push.SendAsync Error");
            }

            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/TodoItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteTodoItem(string id)
        {
            return DeleteAsync(id);
        }
    }
}