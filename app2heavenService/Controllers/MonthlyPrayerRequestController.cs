﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using System.Diagnostics;

using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.Mobile.Server.Config;

using app2heavenService.DataObjects;
using app2heavenService.Models;

namespace app2heavenService.Controllers
{
    public class MonthlyPrayerRequestController : TableController<MonthlyPrayerRequest>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            app2heavenContext context = new app2heavenContext();

            DomainManager = new EntityDomainManager<MonthlyPrayerRequest>(context, Request);
        }

        // GET tables/MonthlyPrayerRequest
        public IQueryable<MonthlyPrayerRequest> GetAllStimuli()
        {
            return Query();
        }

        // GET tables/MonthlyPrayerRequest/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<MonthlyPrayerRequest> GetMonthlyPrayerRequest(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/MonthlyPrayerRequest/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<MonthlyPrayerRequest> PatchMonthlyPrayerRequest(string id, Delta<MonthlyPrayerRequest> patch)
        {
            return UpdateAsync(id, patch);
        }
      
    }
}