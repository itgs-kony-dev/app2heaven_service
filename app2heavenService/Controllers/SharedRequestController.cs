﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using System.Diagnostics;

using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.Mobile.Server.Config;

using app2heavenService.DataObjects;
using app2heavenService.Models;


namespace app2heavenService.Controllers
{
    public class SharedRequestController : TableController<SharedRequest>
    {
        app2heavenContext context;

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            context = new app2heavenContext();

            DomainManager = new EntityDomainManager<SharedRequest>(context, Request);
        }

        // GET tables/MessageItem
        public IQueryable<SharedRequest> GetAllMessageItems()
        {
            return Query();            
        }

        // GET tables/MessageItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<SharedRequest> GetMessageItem(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/MessageItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<SharedRequest> PatchMessageItem(string id, Delta<SharedRequest> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/MessageItem
        public async Task<IHttpActionResult> PostMessageItem(SharedRequest item)
        {
            SharedRequest current = await InsertAsync(item);
            
            Trace.TraceInformation("Adding Request from " + item.From + " " + item.To + ".");

            AccountItem receiver = context
                .AccountItems.Where(account => account.GSM == item.To && account.Deleted != true)
                .FirstOrDefault();

            AccountItem sender = context
                .AccountItems.Where(account => account.GSM == item.From && account.Deleted != true)
                .FirstOrDefault();

            List<string> tags = new List<string>();

            if (receiver != null)
            {
                //Trace.TraceInformation(receiver.Id);
                tags.Add(receiver.Id);
            }

            PushNotifier pn = new PushNotifier(this.Configuration);

            Task.Run(async () =>
            {
                string notif = "{ \"data\" : {\"message\":\"" + item.Title + "\""
                    + ", \"title\":\"" + "Nachricht von " + item.From + "\""
                    + ", \"sender\":\"" + item.From + "\""
                    + ", \"receiver\":\"" + item.To + "\""
                    + ", \"type\":\"" + "newRequest" + "\"";

                notif += "}}";

                //await Task.Delay(5000);

                Trace.TraceInformation("Sending PushNotification: " + notif + " Tags: ");

                foreach (string tag in tags)
                    Trace.TraceInformation(tag);

                await pn.SendPushNotification(notif, tags.ToArray());
            });

            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/MessageItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteMessageItem(string id)
        {
            return DeleteAsync(id);
        }
    }
}