﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using System.Diagnostics;

using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.Mobile.Server.Config;

using app2heavenService.DataObjects;
using app2heavenService.Models;

namespace app2heavenService.Controllers
{
    public class LabelController : TableController<Label>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            app2heavenContext context = new app2heavenContext();

            DomainManager = new EntityDomainManager<Label>(context, Request);
        }
               
        public IQueryable<Label> GetAllLabels()
        {
            return Query();
        }
               
        public SingleResult<Label> GetLabel(string id)
        {
            return Lookup(id);
        }
                
        public Task<Label> PatchLabel(string id, Delta<Label> patch)
        {
            return UpdateAsync(id, patch);
        }
    }
}