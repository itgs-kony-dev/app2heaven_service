﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using System.Diagnostics;

using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.Mobile.Server.Config;

using app2heavenService.DataObjects;
using app2heavenService.Models;

namespace app2heavenService.Controllers
{
    public class StimulusController : TableController<Stimulus>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            app2heavenContext context = new app2heavenContext();

            DomainManager = new EntityDomainManager<Stimulus>(context, Request);
        }

        // GET tables/Stimulus
        public IQueryable<Stimulus> GetAllStimuli()
        {
            return Query();
        }

        // GET tables/Stimulus/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Stimulus> GetStimulus(string id)
        {
            return Lookup(id);
        }

        
        // PATCH tables/Stimulus/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Stimulus> PatchStimulus(string id, Delta<Stimulus> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/Stimulus
        /*public async Task<IHttpActionResult> PostStimulus(Stimulus item)
        {
            Stimulus current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }*/

        // DELETE tables/Stimulus/48D68C86-6EA6-4C25-AA33-223FC9A27959
        /*public Task DeleteStimulus(string id)
        {
            return DeleteAsync(id);
        }*/
         
    }
}