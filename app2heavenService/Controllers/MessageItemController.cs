﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using System.Diagnostics;

using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.Mobile.Server.Config;

using app2heavenService.DataObjects;
using app2heavenService.Models;


namespace app2heavenService.Controllers
{
    public class MessageItemController : TableController<MessageItem>
    {
        app2heavenContext context;

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            context = new app2heavenContext();
            //context.MessageItems.()
            DomainManager = new EntityDomainManager<MessageItem>(context, Request);
        }

        // GET tables/MessageItem
        public IQueryable<MessageItem> GetAllMessageItems()
        {
            /*var deleted = Query().Where(x => x.Text != "John");

            foreach (MessageItem item in deleted)
            {
                item.Deleted = true;
                item.UpdatedAt = DateTimeOffset.Now;
            }*/

            return Query();//.Where(x => x.Text == "John");
            
               // .Union(deleted)
               // .Union(new List<MessageItem> { new MessageItem { Text = "From Service", CreatedAt = DateTimeOffset.Now, Id = Guid.NewGuid().ToString(), UpdatedAt = DateTimeOffset.Now, Version = new byte[] { 8, 239 } } });                
        }

        // GET tables/MessageItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<MessageItem> GetMessageItem(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/MessageItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<MessageItem> PatchMessageItem(string id, Delta<MessageItem> patch)
        {
            return UpdateAsync(id, patch);
        }

        // POST tables/MessageItem
        public async Task<IHttpActionResult> PostMessageItem(MessageItem item)
        {
            MessageItem current = await InsertAsync(item);

            /*// Get the settings for the server project.
            HttpConfiguration config = this.Configuration;
            MobileAppSettingsDictionary settings =
                this.Configuration.GetMobileAppSettingsProvider().GetMobileAppSettings();

            // Get the Notification Hubs credentials for the Mobile App.
            string notificationHubName = settings.NotificationHubName;
            string notificationHubConnection = settings
                .Connections[MobileAppSettingsKeys.NotificationHubConnectionString].ConnectionString;

            // Create a new Notification Hub client.
            NotificationHubClient hub = NotificationHubClient
                .CreateClientFromConnectionString(notificationHubConnection, notificationHubName);

            // Sending the message so that all template registrations that contain "messageParam"
            // will receive the notifications. This includes APNS, GCM, WNS, and MPNS template registrations.
            Dictionary<string, string> templateParams = new Dictionary<string, string>();
            templateParams["messageParam"] = item.Text + " was added to the list.";

            try
            {
                // Send the push notification and log the results.
                //Microsoft.WindowsAzure.MobileServices.Push
                //var result = await hub.SendTemplateNotificationAsync(templateParams, new string [] { "John" });
                var notif = "{ \"data\" : {\"message\":\"" + "From " + "user" + ": " + "message" + "\"}}";
                var result = await hub.SendGcmNativeNotificationAsync(notif, new string[] { item.Text });
            
             * // Write the success result to the logs.
                config.Services.GetTraceWriter().Info(result.State.ToString());
            }
            catch (System.Exception ex)
            {
                // Write the failure result to the logs.
                config.Services.GetTraceWriter()
                    .Error(ex.Message, null, "Push.SendAsync Error");
            }*/

            Trace.TraceInformation("Adding Message from " + item.From + " " + item.To + ".");

            AccountItem receiver = context
                .AccountItems.Where(account => account.GSM == item.To && account.Deleted != true)
                .FirstOrDefault();

            AccountItem sender = context
                .AccountItems.Where(account => account.GSM == item.From && account.Deleted != true)
                .FirstOrDefault();

            List<string> tags = new List<string>();

            if (receiver != null)
            {
                //Trace.TraceInformation(receiver.Id);
                tags.Add(receiver.Id);
            }

            /*if (sender != null)
            {
                Trace.TraceInformation(sender.Id);
                tags.Add(sender.Id);
            }*/

            PushNotifier pn = new PushNotifier(this.Configuration);

            Task.Run(async () =>
            {
                string notif = "{ \"data\" : {\"message\":\"" + item.Text + "\""
                    + ", \"title\":\"" + "Nachricht von " + item.From + "\""
                    + ", \"sender\":\"" + item.From + "\""
                    + ", \"receiver\":\"" + item.To + "\""
                    + ", \"type\":\"" + "newMessage" + "\"";

                notif += "}}";

                //await Task.Delay(5000);

                Trace.TraceInformation("Sending PushNotification: " + notif + " Tags: ");

                foreach (string tag in tags)
                    Trace.TraceInformation(tag);

                await pn.SendPushNotification(notif, tags.ToArray());
            });

            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/MessageItem/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteMessageItem(string id)
        {
            return DeleteAsync(id);
        }
    }
}