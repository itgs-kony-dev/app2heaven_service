﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using System.Diagnostics;

using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.NotificationHubs;
using Microsoft.Azure.Mobile.Server.Config;

using app2heavenService.DataObjects;
using app2heavenService.Models;

namespace app2heaven_dev2Service.Controllers
{
    public class CommunityController : TableController<Community>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            //app2heaven_devContext context = new app2heaven_devContext();
            app2heavenContext context = new app2heavenContext();

            DomainManager = new EntityDomainManager<Community>(context, Request);
        }

        // GET tables/Contributor
        public IQueryable<Community> GetAllContributors()
        {
            return Query();
        }

        // GET tables/Contributor/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Community> GetContributor(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Contributor/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Community> PatchContributor(string id, Delta<Community> patch)
        {
            return UpdateAsync(id, patch);
        }

    }
}