using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(app2heavenService.Startup))]

namespace app2heavenService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureMobileApp(app);
        }
    }
}